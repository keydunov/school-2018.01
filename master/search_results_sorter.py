import collections

from ..core.document_tokenizer import tokenize_string


class SearchResultsSorter:
    def __init__(self, local_files_info, config):
        self._config = config
        self._local_files_info = local_files_info
        # local_files_info = [(url1, content1), (url2, content2)...]

    def sort_by_relevance(self, query):
        tokenized_query = tokenize_string(query)
        unique_tokens_query = collections.Counter(tokenized_query)

        indexed_max_sum = []
        for document in self._local_files_info:
            tokenized_content = tokenize_string(document[1])
            unique_tokens_content = collections.Counter(tokenized_content)

            amount_sum = 0
            for token in unique_tokens_query:
                amount_sum += unique_tokens_content.get(token, 0)

            indexed_max_sum.append((amount_sum, document))
        sorted_indexes = sorted(indexed_max_sum, reverse=True)
        result = [pair[1] for pair in sorted_indexes[:self._config["response_size"]]]
        return result
